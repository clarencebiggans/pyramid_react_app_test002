def includeme(config):
    config.add_static_view('static2', 'static', cache_max_age=3600)
    config.add_static_view('static', 'pyramid_test02:build/static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('home2', '/home2')

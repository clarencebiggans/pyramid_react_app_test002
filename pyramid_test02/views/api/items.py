from pyramid.view import view_config
from pyramid.response import Response

from sqlalchemy.exc import DBAPIError

from pyramid_test02 import models


@view_config(route_name='home2', renderer='pyramid_test02:build/index.jinja2')
def my_view(request):
    return {'one': 'test', 'project': 'pyramid_test02'}



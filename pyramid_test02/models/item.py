from sqlalchemy import (
    Column,
    Index,
    Integer,
    Text,
)

from .meta import Base


class Item(Base):
    __tablename__ = 'items'
    id = Column(Integer, primary_key=True)
    name = Column(Text)
    description = Column(Text)
    value = Column(Integer)


Index('item_index', Item.name, unique=True, mysql_length=255)
